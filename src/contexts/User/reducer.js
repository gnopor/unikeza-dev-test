const setUsers = (state, action) => ({ ...state, users: action.users });

const addUser = (state, action) => {
  const users = [...state.users, action.user];
  return { ...state, users };
};

const updateUser = (state, action) => {
  const users = state.users;
  let index = users.findIndex((user) => user.id === action.user.id);
  users[index] = action.user;
  return { ...state, users };
};

// reducer
const reducer = (state, action) => {
  switch (action.type) {
    case "set_users":
      return setUsers(state, action);

    case "add_user":
      return addUser(state, action);

    case "update_user":
      return updateUser(state, action);

    default:
      return state;
  }
};

// global state
const initialState = {
  users: [],
};

export { reducer, initialState };
