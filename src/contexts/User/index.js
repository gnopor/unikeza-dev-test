import React from "react";
import { reducer, initialState } from "./reducer";

const UsersContext = React.createContext({
  state: initialState,
  dispatch: () => null,
});

const UsersProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <UsersContext.Provider value={[state, dispatch]}>
      {children}
    </UsersContext.Provider>
  );
};

export { UsersContext, UsersProvider };
