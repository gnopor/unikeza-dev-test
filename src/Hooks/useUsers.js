import firebase from "firebase/app";
import "firebase/firestore";

// init firebase

const config = {
  apiKey: process.env.REACT_APP_apiKey,
  authDomain: process.env.REACT_APP_authDomain,
  projectId: process.env.REACT_APP_projectId,
  storageBucket: process.env.REACT_APP_storageBucket,
  messagingSenderId: process.env.REACT_APP_messagingSenderId,
  appId: process.env.REACT_APP_appId,
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
} else {
  firebase.app(); // if already initialized, use that one
}

const db = firebase.firestore();
const usersDB = db.collection("users");

// declare our hook
export default function useUser() {
  const getUser = (users, id) => users.find((user) => user.id === id);

  const formatDate = (d = new Date()) => {
    const date = new Date(d);
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = month.length > 1 ? month : `0${month}`;
    let day = date.getDate();
    day = day.length > 1 ? day : `0${day}`;

    return `${day} / ${month} / ${year}`;
  };

  const getUsers = () =>
    new Promise(async (resolve, reject) => {
      const formatedUsers = [];
      const results = await usersDB.get();
      const length = results.docs.length;

      results.docs.forEach((doc) =>
        formatedUsers.push({ ...doc.data(), id: doc.id })
      );

      const interval = setInterval(() => {
        if (formatedUsers.length === length) {
          clearInterval(interval);
          resolve(formatedUsers);
        }
      }, 100);
    });

  const addUser = async (user) => {
    return await usersDB.doc(user.id).set(user);
  };

  const updateUser = async (user) => {
    return await usersDB.doc(user.id).set(user);
  };

  return { getUser, formatDate, addUser, getUsers, updateUser };
}
