import "./UserCard.css";

function UserCard(props) {
  const { user } = props;
  return (
    <article>
      <div className="card-img">
        <i className="fa fa-user" aria-hidden="true"></i>
      </div>
      <div className="card-body">
        <span>{user.firstName}</span>
        <span>{user.lastName}</span>
      </div>
    </article>
  );
}

export default UserCard;
