import React, { useState } from "react";

import { Link } from "react-router-dom";
import UserForm from "../UserForm/UserForm";

import logo from "./logo.svg";
import "./Header.css";

function Header() {
  const [showForm, setShowForm] = useState(false);

  const toggleForm = (evt) => setShowForm(!showForm);

  return (
    <header>
      <nav className="container">
        <Link to="/">
          <img
            src={logo}
            className="logo"
            width="50px"
            height="50px"
            alt="logo"
          />{" "}
          <span>Home</span>
        </Link>

        <button onClick={toggleForm}>Add User</button>
      </nav>

      {showForm && <UserForm is-edit-form={false} toggleForm={toggleForm} />}
    </header>
  );
}

export default Header;
