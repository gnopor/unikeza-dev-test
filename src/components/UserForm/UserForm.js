import React, { useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";

import { UsersContext } from "../../contexts/User";
import useUsers from "../../Hooks/useUsers";

import "./UserForm.css";

function UserForm(props) {
  const { toggleForm, user } = props;
  const { addUser, updateUser } = useUsers();

  // state and dispatch came from <UserContext.Provider value={[state, dispatch]}></UserContext.Provider>
  // const [state, dispatch] = React.useContext(UsersContext);
  const [, dispatch] = React.useContext(UsersContext);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [age, setAge] = useState(0);
  const [sex, setSex] = useState("");

  useEffect(() => {
    if (props["is-edit-form"]) {
      setFirstName(user.firstName);
      setLastName(user.lastName);
      setAddress(user.address);
      setAge(user.age);
      setSex(user.sex);
    }
  }, [props, user]);

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    const new_user = {
      firstName,
      lastName,
      address,
      age,
      sex,
      date_update: new Date().toString(),
    };

    if (props["is-edit-form"]) {
      const data = { ...new_user, id: user.id };

      await updateUser(data);
      dispatch({ type: "update_user", user: data });
    } else {
      const data = {
        ...new_user,
        id: uuidv4(),
        date_creation: new Date().toString(),
      };
      await addUser(data);
      dispatch({ type: "add_user", user: data });
    }

    toggleForm();
  };

  return (
    <section className="user-form">
      {props["is-edit-form"] ? (
        <h2>Edit user: {user.id} </h2>
      ) : (
        <h2>Add user: </h2>
      )}

      <form>
        <div className="input">
          <label>Nom</label>
          <input
            type="text"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            placeholder="Nom"
          />
        </div>
        <div className="input">
          <label>Prenom</label>
          <input
            type="text"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            placeholder="Prenom"
          />
        </div>
        <div className="input">
          <label>Adresse</label>
          <input
            type="text"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
            placeholder="Adresse"
          />
        </div>
        <div className="input">
          <label>Age</label>
          <input
            type="number"
            value={age}
            onChange={(e) => setAge(e.target.value)}
            placeholder="Age"
          />
        </div>
        <div className="input">
          <label>Sexe</label>
          <input
            type="text"
            value={sex}
            onChange={(e) => setSex(e.target.value)}
            placeholder="Sexe"
          />
        </div>
        <div className="actions">
          <button onClick={toggleForm}>Cancel</button>
          <button onClick={handleSubmit}>Submit</button>
        </div>
      </form>
    </section>
  );
}

export default UserForm;
