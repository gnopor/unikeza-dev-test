import { Route, Switch } from "react-router-dom";

import { UsersProvider } from "./contexts/User";

import Home from "./layouts/Home/Home";
import UserDetail from "./layouts/UserDetail/UserDetail";
import Header from "./components/Header/Header";

import "./App.css";

function App() {
  return (
    <UsersProvider>
      <div className="app">
        <Header />

        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/user/:id" component={UserDetail} />
          {/* <Route component={Error} /> */}
        </Switch>
      </div>
    </UsersProvider>
  );
}

export default App;
