import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";

import UserCard from "../../components/UserCard/UserCard";
import useUsers from "../../Hooks/useUsers";
import { UsersContext } from "../../contexts/User";

import "./Home.css";

function Home() {
  const { getUsers } = useUsers();
  const [state, dispatch] = useContext(UsersContext);

  useEffect(() => {
    state.users.length === 0 && handleSetUsers();
  });

  const handleSetUsers = async () => {
    const users = await getUsers();
    dispatch({ type: "set_users", users });
  };

  return (
    <section className="home">
      {state.users.map((item) => (
        <Link to={`user/${item.id}`} key={item.id}>
          <UserCard user={item} />
        </Link>
      ))}
    </section>
  );
}

export default Home;
