import React, { useState, useEffect, useContext } from "react";
import { useHistory, useParams } from "react-router-dom";
import useUsers from "../../Hooks/useUsers";

import { UsersContext } from "../../contexts/User";

import UserForm from "../../components/UserForm/UserForm";
import "./UserDetail.css";

function UserDetail() {
  let { id } = useParams();
  let history = useHistory();

  const [state] = useContext(UsersContext);

  // TODOS: rename this hook
  const { getUser, formatDate } = useUsers();
  const [showForm, setShowForm] = useState(false);
  const [user, setUser] = useState({});

  useEffect(() => {
    const user = getUser(state.users, id);
    if (user) {
      setUser(user);
    } else {
      history.push("/");
    }
  }, [id, history, getUser, state]);

  const toggleForm = (evt) => setShowForm(!showForm);

  return (
    <section className="detail-page">
      {showForm && (
        <UserForm is-edit-form toggleForm={toggleForm} user={user} />
      )}
      Hi this is the detail page : {id}
      <div>
        <span>
          Nom: <span>{user.firstName}</span>
        </span>
        <span>
          Prenom: <span>{user.lastName}</span>
        </span>
        <span>
          Sexe: <span>{user.sex}</span>
        </span>
        <span>
          Age: <span>{user.age}</span>
        </span>
        <span>
          Adresse: <span>{user.address}</span>
        </span>
        <span>
          Date Ajout: <span>{formatDate(user.date_creation)}</span>
        </span>
        <span>
          Date Update: <span>{formatDate(user.date_update)}</span>
        </span>
      </div>
      <button onClick={toggleForm}>Update</button>
    </section>
  );
}

export default UserDetail;
